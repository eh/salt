vcs:
  pkg.installed:
    - pkgs:
      - git
      - git-doc
      - gitk
      - tig
      - subversion
      - subversion-tools
