grub-config-append-max_cstate:
  file.replace:
    - name: /etc/default/grub
    - pattern: 'GRUB_CMDLINE_LINUX_DEFAULT="quiet"'
    - repl: 'GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_idle.max_cstate=4"'

update-grub-after-append-max_cstate:
  cmd.run:
    - name: update-grub
    - require:
      - grub-config-append-max_cstate
