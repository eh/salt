edit-config-timeout:
  file.replace:
    - name: /etc/default/grub
    - pattern: 'GRUB_TIMEOUT=5'
    - repl: 'GRUB_TIMEOUT=3'

update-grub-after-config-timeout:
  cmd.run:
    - name: update-grub
    - require:
      - edit-config-timeout
