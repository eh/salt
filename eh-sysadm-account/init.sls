# See https://wiki.debian.org/SystemGroups
eh:
  user.present:
    - groups:
      - lp
      - lpadmin
      - scanner
      - adm
      - systemd-journal
      - plugdev
      - netdev
      - cdrom
      - audio
      - video
      - sudo
      - staff
      - dialout
      - libvirt
      - libvirt-qemu
