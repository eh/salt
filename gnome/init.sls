gnome:
  pkg.installed:
    - pkgs:
      - task-gnome-desktop
      - task-french
      - task-french-desktop
      - dconf-editor
      - remmina
