mail:
  pkg.installed:
    - pkgs:
      - mutt
      - post-el
      - abook
      - lbdb
      - libnet-ldap-perl
      - offlineimap
      - notmuch-mutt
      - rt4-clients
      - paps
      - thunderbird-l10n-fr
