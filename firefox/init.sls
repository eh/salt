firefox:
  pkg.installed:
    - fromrepo: unstable
    - pkgs:
      - firefox-l10n-fr
      - webext-ublock-origin
      - ublock-origin-doc

