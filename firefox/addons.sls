# Script to install addon
/usr/local/sbin/install-firefox-addon:
  file.managed:
    - source: salt:///firefox/install-firefox-addon
    - user: root
    - group: root
    - mode: 755

# VimFX (404785)
/usr/local/sbin/install-firefox-addon 404785:
  cmd.run:
    - require:
      - file: /usr/local/sbin/install-firefox-addon

# FoxyProxy (400263)
/usr/local/sbin/install-firefox-addon 400263 foxyproxy_standard-4.5.6-fx+sm+tb.xpi:
  cmd.run:
    - require:
      - file: /usr/local/sbin/install-firefox-addon

# Google redirects fixer (371815)
/usr/local/sbin/install-firefox-addon 371815:
  cmd.run:
    - require:
      - file: /usr/local/sbin/install-firefox-addon

# It's all text (354500)
/usr/local/sbin/install-firefox-addon 354500 its_all_text-1.9.2-sm+fx.xpi:
  cmd.run:
    - require:
      - file: /usr/local/sbin/install-firefox-addon

# uBlock Origin (607454)
/usr/local/sbin/install-firefox-addon 607454:
  cmd.run:
    - require:
      - file: /usr/local/sbin/install-firefox-addon

