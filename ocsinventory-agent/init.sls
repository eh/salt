ocsinventory-agent-debconf:
  debconf.set:
    - name: ocsinventory-agent
    - data:
        'ocsinventory-agent/method': {'type': 'select', 'value': 'http'}
        'ocsinventory-agent/server': {'type': 'string', 'value': 'http://bar.obspm.fr/ocsinventory'}

ocsinventory-agent:
  pkg.installed:
    - reinstall: True
    - onchanges:
      - debconf: ocsinventory-agent-debconf

