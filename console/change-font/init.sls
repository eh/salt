change-console-font-codeset:
  file.replace:
    - name: /etc/default/console-setup
    - pattern: 'CODESET=.*'
    - repl: 'CODESET="Uni2"'

change-console-font-fontface:
  file.replace:
    - name: /etc/default/console-setup
    - pattern: 'FONTFACE=.*'
    - repl: 'FONTFACE="TerminusBold"'

change-console-font-fontsize:
  file.replace:
    - name: /etc/default/console-setup
    - pattern: 'FONTSIZE=.*'
    - repl: 'FONTSIZE="12x24"'
