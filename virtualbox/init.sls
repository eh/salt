virtualbox:
  pkgrepo.managed:
    - humanname: Virtualbox unofficial repo
    - name: deb http://download.virtualbox.org/virtualbox/debian buster contrib
    - dist: buster
    - file: /etc/apt/sources.list.d/virtualbox.list
    - require_in:
      - pkg: virtualbox

  pkg.latest:
    - name: virtualbox
    - name: virtualbox-guest-dkms
    - refresh: True
