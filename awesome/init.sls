awesome:
  pkg.installed:
    - pkgs:
      - awesome
      - awesome-doc
      - lua5.3
      - lua5.2-doc
      - ssh-askpass-gnome
      - autorandr
      - arandr
      - brightnessctl
      - nagstamon
      - pavucontrol
      - pasystray
      - udiskie
      - gnome-screensaver
      - zsh

rxvt-unicode-256color:
  pkg.installed: []

update-alternatives --set x-terminal-emulator /usr/bin/urxvt:
  cmd.run:
    - require:
      - pkg: rxvt-unicode-256color
