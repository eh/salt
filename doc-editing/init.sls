doc-editing:
  pkg.installed:
    - pkgs:
      - inkscape
      - inkscape-tutorials
      - inkscape-open-symbols
      - scrot
      - qelectrotech
      - graphviz
      - graphviz-doc
      - dia
