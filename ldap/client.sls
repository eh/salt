configure-ldap-base:
  file.replace:
    - name: /etc/ldap/ldap.conf
    - pattern: '^#BASE	dc=example,dc=com$'
    - repl:    'BASE	dc=obspm,dc=fr'

configure-ldap-uri:
  file.replace:
    - name: /etc/ldap/ldap.conf
    - pattern: '^#URI	ldap://ldap.example.com ldap://ldap-master.example.com:666$'
    - repl:    'URI	ldap://ldap-ext.obspm.fr'
