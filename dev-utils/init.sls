dev-utils:
  pkg.installed:
    - pkgs:
      - gettext-doc
    - pkgs:
      - python3-pip
      - python3-pylint-common
      - virtualenv
