kvm:
  pkg.installed:
    - pkgs:
      - qemu-kvm
      - libvirt-clients
      - libvirt-daemon-system
      - virt-manager
