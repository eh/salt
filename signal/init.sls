signal:
  pkgrepo.managed:
    - humanname: Signal unofficial repo
    - name: deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main
    - dist: xenial
    - file: /etc/apt/sources.list.d/signal-xenial.list
    - require_in:
      - pkg: signal-desktop

  pkg.latest:
    - name: signal-desktop
    - refresh: True

/opt/Signal/chrome-sandbox:
  file.managed:
    - mode: 4755
    - require:
      - signal
