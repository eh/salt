cli-network-utils:
  pkg.installed:
    - pkgs:
      - bridge-utils
      - geoip-bin
      - iftop
      - ipcalc
      - lftp
      - lldpd
      - minicom
      - mtr-tiny
      - nmap
      - sipcalc
      - snmp
      - tcptrack
      - tshark
      - w3m
      - wavemon

dialout:
  group.present:
    - addusers:
      - eh
    - require:
      - pkg: cli-network-utils