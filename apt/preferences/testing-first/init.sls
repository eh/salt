# Install file into /etc/apt/preferences.d/
/etc/apt/preferences.d/testing-first:
  file.managed:
    - source: salt://apt/preferences/testing-first/preferences
    - user: root
    - group: root
    - mode: '0755'
