# Assure un /etc/apt/sources.list vide
/etc/apt/sources.list:
  file.managed:
    - contents: ''
