sources-testing:
  pkgrepo.managed:
    - humanname: Debian testing
    - name: deb http://ftp.fr.debian.org/debian testing main contrib non-free
    - dist: testing
    - file: /etc/apt/sources.list.d/testing.list
