latex:
  pkg.installed:
    - pkgs:
      - texlive
      - texlive-lang-french
      - texlive-extra-utils
      - texlive-latex-base-doc
      - texlive-latex-extra
      - texlive-latex-extra-doc
