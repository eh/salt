security-utils:
  pkg.installed:
    - pkgs:
      - keepass2
      - keepass2-doc
      - kpcli
      - pwgen
